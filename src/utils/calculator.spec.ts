import addTwoMultiplyByThree from "./calculator";

test("needs should only include existing features", () => {
  const result = addTwoMultiplyByThree(10);
  expect(result).toBe(36);
});
