import AbstractContent from "./AbstractContent";

export default class SomeContent implements AbstractContent {
  title = cy.contains("OMG this is amazing");
  content = cy.contains("I love testing because");
}
