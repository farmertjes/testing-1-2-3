export class ClickToAdd {
  addButton = "Click to add";
  resetButton = "reset";
  resultDiv = () => {
    return cy.get("#currentNumber");
  };

  clickAdd(): void {
    cy.contains(this.addButton).click();
  }

  clickReset(): void {
    cy.contains(this.resetButton).click();
  }
}
