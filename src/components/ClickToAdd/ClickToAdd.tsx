import { useState } from "react";
import "./ClickToAdd.css";
import addTwoMultiplyByThree from "../../utils/calculator";

export function ClickToAdd(): JSX.Element {
  const [currentNumber, setCurrentNumber] = useState(0);

  function didClick() {
    const newNumber = addTwoMultiplyByThree(currentNumber);
    setCurrentNumber(newNumber);
  }

  function reset() {
    setCurrentNumber(0);
  }

  return (
    <>
      <div>
        <h2>Click me Elmo</h2>
        <div
          id="currentNumber"
          className={currentNumber > 1000 ? "tooHigh" : "allGood"}
        >
          {currentNumber}
        </div>
        <button onClick={didClick}>Click to add</button>
        <button onClick={reset}>reset</button>
      </div>
    </>
  );
}
