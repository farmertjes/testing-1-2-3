import { ClickToAdd } from "./ClickToAdd";
import ContentSwitcher from "./ContentSwitcher";

export default class App {
  contentSwitcher = new ContentSwitcher();
  clickToAdd = new ClickToAdd();
}
