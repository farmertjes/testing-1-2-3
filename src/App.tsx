import logo from './logo.svg';
import './App.css';
import { ClickToAdd } from './components/ClickToAdd/ClickToAdd';
import ContentSwitcher from './components/ContentSwitcher/ContentSwicher';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          <ClickToAdd />
        </p>
        <p>
          <ContentSwitcher></ContentSwitcher>
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
