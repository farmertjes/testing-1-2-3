import { useState } from "react"
import { SomeContent } from "../SomeContent/SomeContent"
import SomeOtherContent from "../SomeOtherContent/SomeOtherContent"

export default function ContentSwitcher():JSX.Element {

    const [currentContent, setCurrentContent] = useState<'some'|'other'>('some')


    return <>
        <button onClick={()=>{setCurrentContent('some')}}>some</button><button onClick={()=>{setCurrentContent('other')}}>other</button>
        {currentContent === 'some' ? (<SomeContent></SomeContent>) :(<SomeOtherContent></SomeOtherContent>)}
    </>
}