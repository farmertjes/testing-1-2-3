import cypress from "cypress";
import AbstractContent from "../models/AbstractContent";
import App from "../models/App";

describe("App", () => {
  it("should switch content", () => {
    cy.visit("http://localhost:3001");
    const app = new App();
    app.clickToAdd.resultDiv().should("be.visible");

    const content: AbstractContent = app.contentSwitcher.switchToOther();
    content.title.should("be.visible");
    cy.percySnapshot();
  });

  it("should switch back", () => {
    const app = new App();
    const content = app.contentSwitcher.switchToSome();
    content.title.should("be.visible");
    cy.percySnapshot();
  });

  it("should start by showing green color", () => {
    const app = new App();
    app.clickToAdd.resultDiv().should("have.class", "allGood");
    cy.percySnapshot();
  });

  it("change the color depending on the value", () => {
    const app = new App();

    for (let i = 0; i < 7; i++) {
      app.clickToAdd.clickAdd();
    }
    app.clickToAdd.resultDiv().should("have.class", "tooHigh");

    app.clickToAdd.clickReset();
    app.clickToAdd.resultDiv().should("have.class", "allGood");
    cy.percySnapshot();
  });
});
