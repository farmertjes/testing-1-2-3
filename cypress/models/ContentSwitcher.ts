import SomeContent from "./SomeContent";
import SomeOtherContent from "./SomeOtherContent";

export default class ContentSwitcher {
  someButton = "some";
  otherButton = "other";

  switchToSome() {
    cy.contains(this.someButton).should("be.visible");
    cy.contains(this.someButton).click();
    return new SomeContent();
  }

  switchToOther() {
    cy.contains(this.otherButton).should("be.visible");
    cy.contains(this.otherButton).click();
    return new SomeOtherContent();
  }
}
