export default abstract class BaseContent {
  title: Cypress.Chainable;
  content: Cypress.Chainable;
}
