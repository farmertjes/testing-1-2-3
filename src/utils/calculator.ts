export default function addTwoMultiplyByThree(number: number) {
    return (number + 2) * 3;
}