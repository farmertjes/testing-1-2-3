import AbstractContent from "./AbstractContent";

export default class SomeOtherContent implements AbstractContent {
  title = cy.contains("OMG this is terrible");
  content = cy.contains("I hate testing because");
}
